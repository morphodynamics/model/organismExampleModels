This project includes example model files to run a set of example models.

Each model will have its own directory under which an organism and one tissue directory holds the model files.

Several of the example models originate from the Sainsbury Computatinal Workshop where more information on the specific models, including related papers, described <a href=https://gitlab.com/morphodynamics/model/documentation/wikis/home>here</a>.